import { Injectable } from '@angular/core';
import { Products } from '../components/models/products';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  products: Products[] = [

    /*     new Product (1, 'Product 1', 'This in the product 1 description. The product is really kool', 100),
        new Product (2, 'Product 2', 'This in the product 2 description. The product is really kool', 50),
        new Product (3, 'Product 3', 'This in the product 3 description. The product is really kool', 70),
        new Product (4, 'Product 4', 'This in the product 4 description. The product is really kool', 200),
        new Product (5, 'Product 5', 'This in the product 5 description. The product is really kool', 150),
        new Product (6, 'Product 6', 'This in the product 6 description. The product is really kool', 50),
        new Product (7, 'Product 7', 'This in the product 7 description. The product is really kool', 50), */
     
    
        {
          id: 1,
          nombre: "Sudadera sin capucha",
          ingredientes: "PANKO TORI, pollo queso cebolli, EBI ROLL, camaron queso cebollin, california kani, kanicama palta en ciboulette, crispi teri white, pollo furay palta cebollin en queso.",
          img: "assets/modamens.jpeg",
          qty: 4,
          precio: 79.99
        },
        {
          id: 2,
          nombre: "Sudadera con capucha invierno",
          ingredientes: "PANKO TORI, pollo queso cebolli, EBI ROLL, camaron queso cebollin, california kani, kanicama palta en ciboulette, crispi teri white, pollo furay palta cebollin en queso.",
          img: "assets/modamens_01.jpg",
          qty: 3,
          precio:  120.00
        },
        {
    
          id: 3,
          nombre: "Saco Gabardina aspect formal",
          ingredientes: "PANKO TORI, pollo queso cebolli, EBI ROLL, camaron queso cebollin, california kani, kanicama palta en ciboulette, crispi teri white, pollo furay palta cebollin en queso.",
          img: "assets/modamens_02.jpeg",
          qty: 2,
          precio: 109.99
        },
        {
          id: 4,
          nombre: "Sudadera Fiesta Black platinium",
          ingredientes: "PANKO TORI, pollo queso cebolli, EBI ROLL, camaron queso cebollin, california kani, kanicama palta en ciboulette, crispi teri white, pollo furay palta cebollin en queso.",
          img: "assets/modamens_03.jpeg",
          qty: 1,
          precio: 270.00
        },
      ]
    
      constructor() { }
    
    getProducts(): Products[]{
      return this.products
    }
    
    
    /*   getProducts() {
        return this.products;
      }
      getPetition(idx: string) {
        return this.products[idx];
      } */
    
    }

  

