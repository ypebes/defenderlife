import { Component, OnInit } from '@angular/core';
import { CartItem } from 'src/app/components/models/cart-item';
import { MessageService } from 'src/app/services/message.service';
import { Products } from '../models/products';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  cartItems = [];

  constructor( private message: MessageService) { }

  ngOnInit(): void {

  }


}
