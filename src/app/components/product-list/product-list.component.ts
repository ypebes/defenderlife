import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/services/products.service';
import { Products } from '../models/products';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  products: Products[] = [];

  constructor(private productsService: ProductsService) { }

  ngOnInit(): void {

    this.loadProducts();
  }
  loadProducts(){
    this.products  =  this.productsService.getProducts();
  }

}
