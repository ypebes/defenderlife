export class Products {

    id: number;
    nombre: string;
    ingredientes: string;
    precio: number;
    qty: number;
    img: string;
    
    
     constructor(id, nombre, ingredientes, precio , img){
            this.id = id;
            this.nombre = nombre;
            this.ingredientes = ingredientes;
            this.precio = precio;
            this.img = img;
        }
    }
    
