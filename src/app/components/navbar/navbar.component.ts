import { Component, OnInit } from '@angular/core';
import { MessageService } from '../../services/message.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

/*   @Input() messageService : MessageService; */
  hidden = false;
  cartBadge: number = 0;

  toggleBadgeVisibility() {

    this.hidden = !this.hidden;
  }
  constructor( private messageService: MessageService) {

    this.messageService.cartSubject.subscribe((data) =>{
      this.cartBadge = data;
    });
   }

  ngOnInit(): void {
    this.cartItemFunc();
  }

  cartItemFunc(){
    if(localStorage.getItem('cart') != null){
      var cartCount = JSON.parse(localStorage.getItem('cart'));
      this.cartBadge = cartCount.length;
    }

  
  }

}
