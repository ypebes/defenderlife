import { Products } from "./products";

export class CartItem {
 
productId: number;
productNombre: string;
productIngredientes: string;
productPrecio: number;
qty : number;


 constructor( product: Products ){
        this.productId = product.id
        this.productNombre = product.nombre
        this.productIngredientes = product.ingredientes
        this.productPrecio = product.precio
        this.qty = 1;
    }
}
