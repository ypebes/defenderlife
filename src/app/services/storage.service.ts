import { Injectable } from '@angular/core';
import { CartItem } from '../components/models/cart-item';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }
  existCart(): boolean{
    return localStorage.getItem('cart') != null;
  }
  setCart(cart : CartItem[]): void{
    localStorage.setItem('cart', JSON.stringify(cart));
  }
  getCart(): CartItem[]{
    return JSON.parse(localStorage.getItem('cart'));
  }
  clear(): void{
    localStorage.removeItem('cart');
  }
}