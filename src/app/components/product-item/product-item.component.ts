import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MessageService } from 'src/app/services/message.service';
import { Products } from '../models/products';
import { SnackComponent } from '../snack/snack.component';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss']
})
export class ProductItemComponent implements OnInit {
  durationInSeconds = 2;
@Input() product: Products;

  constructor(
              private messageService: MessageService,
              private _snackBar: MatSnackBar) { }

  ngOnInit(): void {

  }

  AddToCart():void {
    
    this.messageService.sendMessage(this.product);
    this._snackBar.openFromComponent(SnackComponent, {
      duration: this.durationInSeconds * 1000,
    });
  
    this.cartNumberFunc();
  }
  cartNumber : number = 0;

  cartNumberFunc(){
    var cartValue = JSON.parse(localStorage.getItem('cart'));
    this.cartNumber = cartValue.length;
    this.messageService.cartSubject.next(this.cartNumber);
    
  }
}
