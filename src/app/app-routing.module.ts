import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ShopIconComponent } from './components/shop-icon/shop-icon.component';

const routes: Routes = [
  { path : 'shopIcon', component: ShopIconComponent},
  { path : 'home', component: HomeComponent},

  {path : '', component:HomeComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
