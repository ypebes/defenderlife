import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';
import { Products } from '../components/models/products';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  message = new Subject();

  constructor() { }

  sendMessage(product: Products) {
    this.message.next(product);
  }
  getMessage(): Observable<any> {
    return this.message.asObservable();
  }

 cartSubject = new Subject<any>();
}
