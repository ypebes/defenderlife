import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { MessageService } from 'src/app/services/message.service';
import { StorageService } from 'src/app/services/storage.service';
import { CartItem } from '../models/cart-item';
import { Products } from '../models/products';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  
  @Input() cartItem: CartItem;

  cartItems = [];
  total = 0;

  constructor( private message: MessageService,
                private storageService : StorageService)
                 { }

  ngOnInit(): void {

    if(this.storageService.existCart()){
      this.cartItems= this.storageService.getCart();
    }

    this.getItem();

    this.total = this.getTotal();
  }

  getItem(): void{

    this.message.getMessage().subscribe((product: Products)=>{
      let exists = false;
      this.cartItems.forEach(item =>{
        if(item.productId=== product.id){
          exists = true;
          item.qty++
        }
      });
      if(!exists){
        const carItem = new CartItem(product);
        this.cartItems.push(carItem)
      }
      this.total = this.getTotal();
      this.storageService.setCart(this.cartItems);

    });
  }
  getTotal(): number{
    let total = 0;
    this.cartItems.forEach(item =>{
      total += item.qty * item.productPrecio;
    });
    return +total.toFixed(2);

  }
  emptyCart():void{

    this.cartItems = [];
    this.total = 0;
    this.storageService.clear();

  }
  deleteItem(i : number ):void{
    if(this.cartItems[i].qty > 1){

      this.cartItems[i].qty--;

    }else{
      this.cartItems.splice(i, 1)
    }
    this.total = this.getTotal();
    this.storageService.setCart(this.cartItems);
  }
  addItem(i : number ): void{
    if(this.cartItems[i].qty > 0 ){

      this.cartItems[i].qty++ > 0;

    }else{
      this.cartItems.slice(i, 1)
    }
    this.total = this.getTotal();
    this.storageService.setCart(this.cartItems);
  }
}
